FROM ubuntu:noble-20240114
MAINTAINER sbrown1992@gmail.com

WORKDIR /app

COPY gocademy /app/gocademy
COPY static /app/static

CMD ["./gocademy"]

EXPOSE 8075
