# Gocademy

## Knowledge Principles

1. No matter when you stop engaging with Gocademy, there will always be a path shown to get you
up to date
1. Flashcards will be formatted acording to Supermemo's [20 rules of formulating knowledge](https://www.supermemo.com/en/blog/twenty-rules-of-formulating-knowledge)

## Technical Principles

1. User data will not be sent to the server
1. It will be as easy as possible for users to fork / self-host, if they wish

Note that as a consequence of user data never being sent to the server, users are responsible
for the backup and restoration of their data. We will make use of Web Storage (likely Indexed DB)
to store data between sessions, but responsibilty for backup and restore falls onto end users.
