VERSION=0.0.2
all:
	make verify
	make build

verify:
	go fmt ./...
	go vet ./...
	go mod tidy
	go test -count=1 ./...
	make -s verify-html

build:
	go build -o gocademy

docker:
	docker build -t gocademy .
	docker run -p 8075:8075 gocademy

dependencies:
	go mod download
	npm install

verify-html:
	npx prettier static/*.html --write
	java -jar ./node_modules/vnu-jar/build/dist/vnu.jar --errors-only static/*.html

.PHONY: deploy
deploy:
	docker build -t sbrown1992/gocademy:${VERSION} .
	docker push sbrown1992/gocademy:${VERSION}
	./deploy.sh ${VERSION}
