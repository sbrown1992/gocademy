package main

import (
	"fmt"
	"log"
	"net/http"
)

const (
	port = 8075
)

func main() {
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/", fs)

	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		log.Fatal(err)
	}
}
