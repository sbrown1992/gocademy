HOST=65.108.145.113
VERSION=$1

ssh sbrown@${HOST} VERSION=${VERSION} 'bash -s' <<'ENDSSH'
docker stop gocademy || true
docker rm gocademy || true
docker run -d -p 8075:8075 --name gocademy sbrown1992/gocademy:${VERSION}
ENDSSH

